function icheck() {
    if ($(".icheck").length > 0) {
        $(".icheck").each(function () {
            var $el = $(this);
            var skin = ($el.attr('data-skin') !== undefined) ? "_" + $el.attr('data-skin') : "",
                color = ($el.attr('data-color') !== undefined) ? "-" + $el.attr('data-color') : "";
            var opt = {
                checkboxClass: 'icheckbox' + skin + color,
                radioClass: 'iradio' + skin + color,
            }
            $el.iCheck(opt);
        });
    }

    var resile = $('#contrat_resile');
    resile.on('ifChecked ', function(event){
        $('#contrat_cause_resile').prop('readonly', false);
        //console.log('Checked');
    });
    resile.on('ifUnchecked', function(event){
        $('#contrat_cause_resile').prop('readonly', true);
        //$('#cause_resile').text("");
        //console.log('UnChecked');
    });
}

