

ready = function(){
    icheck();
    sidebar();


    $('.selectpicker').selectpicker();
    $('input[type="tel"]').inputmask({"mask": "9999 999 999"});

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    $('.select2').select2({
        theme: "bootstrap"
    });
    $.fn.modal.Constructor.prototype.enforceFocus = function () {};

    $('.datepicker, .input-group.date').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy'
    });
    $('.dropdown-toggle').dropdown();
    $('.datatable').DataTable({
        retrieve: true,
        "bStateSave": true,
        "bSortClasses": false,
        //"sScrollY": "450px",
        "aoColumnDefs" : [{
            "bSortable" : false,
            "aTargets" : [ "no-sort" ]
        }],
        "oLanguage":{
            "sProcessing":     "Traitement en cours...",
            "sSearch":         "Rechercher&nbsp;:",
            "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
            "sInfo":           " _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            "sInfoEmpty":      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            "sInfoPostFix":    "",
            "sLoadingRecords": "Chargement en cours...",
            "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            "sEmptyTable":     "Aucune donnée disponible dans le tableau",
            "oPaginate": {
                "sFirst":      "Premier",
                "sPrevious":   "Précédent",
                "sNext":       "Suivant",
                "sLast":       "Dernier"
            },
            "oAria": {
                "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });
    $(".dataTables_length select").addClass("form-control");
    $(".dataTables_filter input").addClass("form-control");
    $.fn.dataTable.moment('dd/mm/yyyy');


    $('table.sortable').ready(function(){
        var td_count = $(this).find('tbody tr:first-child td').length
        $('table.sortable tbody').sortable(
            {
                handle: '.handle',
                placeholder: 'ui-sortable-placeholder',
                update: function(event, ui) {
                    $("#progress").show();
                    positions = {};
                    $.each($('table.sortable tbody tr'), function(position, obj){
                        reg = /(\w+_?)+_(\d+)/;
                        parts = reg.exec($(obj).attr('id'));
                        if (parts) {
                            positions['positions['+parts[2]+']'] = position;
                        }
                    });
                    $.ajax({
                        type: 'POST',
                        dataType: 'script',
                        url: $(ui.item).closest("table.sortable").data("sortable-link"),
                        data: positions,
                        success: function(data){ $("#progress").hide(); }
                    });
                },
                start: function (event, ui) {
                    // Set correct height for placehoder (from dragged tr)
                    ui.placeholder.height(ui.item.height())
                    // Fix placeholder content to make it correct width
                    ui.placeholder.html("<td colspan='"+(td_count-1)+"'></td><td class='actions'></td>")
                },
                stop: function (event, ui) {
                    // Fix odd/even classes after reorder
                    $("table.sortable tr:even").removeClass("odd even").addClass("even");
                    $("table.sortable tr:odd").removeClass("odd even").addClass("odd");
                }

            });
    });





}


$(document).ready(ready);
$(document).on('document:load', ready);
