json.array!(@type_contrats) do |type_contrat|
  json.extract! type_contrat, :id, :nom
  json.url type_contrat_url(type_contrat, format: :json)
end
