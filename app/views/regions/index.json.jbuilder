json.array!(@regions) do |region|
  json.extract! region, :id, :nom
  json.url region_url(region, format: :json)
end
