json.array!(@contrats) do |contrat|
  json.extract! contrat, :id, :numero, :date_creation, :date_debut, :date_fin, :main_oeuvre, :nombre_incidents, :piece_rechange, :montant, :resile, :cause_resile, :type_contrat_id, :client_id, :contrat_id
  json.url contrat_url(contrat, format: :json)
end
