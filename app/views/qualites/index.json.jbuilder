json.array!(@qualites) do |qualite|
  json.extract! qualite, :id, :nom
  json.url qualite_url(qualite, format: :json)
end
