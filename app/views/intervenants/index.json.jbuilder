json.array!(@intervenants) do |intervenant|
  json.extract! intervenant, :id, :code, :nom, :adresse, :telephone, :email, :cout_horaire, :mot_de_passe, :date_embauche, :qualite_id, :specialite_id, :region_id
  json.url intervenant_url(intervenant, format: :json)
end
