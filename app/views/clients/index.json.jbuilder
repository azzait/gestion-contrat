json.array!(@clients) do |client|
  json.extract! client, :id, :code, :raison_social, :adresse, :telephone, :fax, :contact, :email, :region_id
  json.url client_url(client, format: :json)
end
