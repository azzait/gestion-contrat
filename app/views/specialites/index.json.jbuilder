json.array!(@specialites) do |specialite|
  json.extract! specialite, :id, :nom
  json.url specialite_url(specialite, format: :json)
end
