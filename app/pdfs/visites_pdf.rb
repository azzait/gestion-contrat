# encoding: utf-8

class VisitesPDF < Prawn::Document

  DOCUMENT_LINES_COUNT = 29
  DOCUMENT_TABLE_CELLS_HEIGHT = 16
  DOCUMENT_TABLE_BORDER_WIDTH = 0.5
  DOCUMENT_TABLE_ALTERNATE_COLORS = nil #["F1F0F2", "FFFFFF"]

   def initialize(contrats, view)
     super(:page_size => "A4", :page_layout => :landscape)
     doc_header

     @contrats = contrats
     @view = view

     line_contrats
     doc_footer
   end

   def doc_header
     # rdraw_text doc_header, :at => bounds.top_left
     font_size(9)

     data = [[ "DYNA INFO", I18n.l(Date.today)]]

     line_width 0.5
     table(data, :width => bounds.width, :cell_style => {:height => 18 }) do
       cells.borders = []
       columns(1).align = :right
     end
     stroke_horizontal_rule
     move_down(5)
     text("Liste Des Visites Préventives", :align => :center, :style => :bold)
     # move_down(5)
   end

  def doc_footer
    repeat(:all) do
      bounding_box [bounds.left, bounds.bottom + 5], :width  => bounds.width do
        font "Helvetica"
        line_width 0.5
        stroke_horizontal_rule
        move_down(15)
        number_pages "DYNA-GC, un produit de Dyna Info.", :size => 8, :align => :left,:at => [0, 10]
      end

    end

    number_pages "Page <page> / <total>",
                 { :start_count_at => 1, :page_filter => :all,:at => [bounds.right - 40, 0],
                   :size => 8}
  end

   def line_contrats
    #,:cell_style => {:border_lines => [:dotted, :solid, nil]}
    font_size(7)
    table(line_contrat_rows, :width => bounds.width, :cell_style => {:height => DOCUMENT_TABLE_CELLS_HEIGHT}) do
      row(0).font_style = :bold
      cells.border_width = DOCUMENT_TABLE_BORDER_WIDTH
      row(DOCUMENT_LINES_COUNT).borders = [:right, :bottom, :left]

      # cells[4, 10].background_color = "DD4B39"
      # cells[4, 10].text_color = "FFFFFF"
      columns(11).align = :right
      cells[4, 11].borders = [:right]
      self.row_colors = DOCUMENT_TABLE_ALTERNATE_COLORS
      self.header = true
    end
  end

  def line_contrat_rows
     # font_size()
     a = months_of_a_year(Date.today.year)
     @contrats.map do |contrat|
       a += [contrat.contrat_line]
     end
     # (1..DOCUMENT_LINES_COUNT - @contrats.count).map do |i|
     #   a += [["", "", "", "", "", "", "", "", "", "", "", "", "", "", ""]]
     # end
    a
  end

  def price(number)
    @view.number_to_currency(number)
  end

  def months_of_a_year(year)
    # move_down(5)
    date_from  = DateTime.new(year).beginning_of_year
    date_to    = DateTime.new(year).end_of_year
    date_range = date_from..date_to

    date_months = date_range.map {|d| Date.new(d.year, d.month, 1) }.uniq
    # date_months = date_months.map {|d| d.to_formatted_s(:month_and_year) }
    date_months = date_months.map do |d|
      I18n.l(d, :format => "%b %Y").capitalize
    end
    date_months = ["Client", "Intervenant", "Contact"].concat(date_months)
    [date_months]
  end

end