# encoding: utf-8
include ActionView::Helpers::TextHelper
class ContratsPDF < Prawn::Document

  DOCUMENT_LINES_COUNT = 24
  DOCUMENT_TABLE_CELLS_HEIGHT = 19
  DOCUMENT_TABLE_BORDER_WIDTH = 0.5
  DOCUMENT_TABLE_ALTERNATE_COLORS = nil #["F1F0F2", "FFFFFF"]

  def initialize(contrats, view)
   super(:page_size => "A4", :page_layout => :landscape)

   @contrats = contrats
   @view = view

   calc_number_of_pages

   doc_header
   line_contrats
   doc_footer
  end

  def doc_header
   font_size(9)

   data = [[ "DYNA INFO", I18n.l(Date.today)]]
   table(data, :width => bounds.width) do
     cells.borders = [:bottom]
     columns(1).align = :right
   end
   move_down(5)
   text("Liste Des Contrats", :align => :center, :style => :bold)
   # move_down(5)

  end

  def doc_footer
    repeat(:all) do
     bounding_box [bounds.left, bounds.bottom + 5], :width  => bounds.width do
       font "Helvetica"
       line_width 0.5
       stroke_horizontal_rule
       move_down(15)
       number_pages "DYNA-GC, un produit de Dyna Info.", :size => 8, :align => :left,:at => [0, 10]
     end
    end

    number_pages "Page <page> / <total>",
                { :start_count_at => 1, :page_filter => :all,:at => [bounds.right - 40, 0],
                  :size => 8}
  end

  def line_contrats
     #,:cell_style => {:border_lines => [:dotted, :solid, nil]}

     @@pc = DOCUMENT_LINES_COUNT * @@total_pages
    table(line_contrat_rows, :width => bounds.width, :cell_style => {:height => DOCUMENT_TABLE_CELLS_HEIGHT}) do
      row(0).font_style = :bold
      cells.borders = [:left, :right]
      cells.border_width = DOCUMENT_TABLE_BORDER_WIDTH
      row(0).borders = [:top, :right, :bottom, :left]
      row(DOCUMENT_LINES_COUNT).borders = [:right, :bottom, :left]
      row(@@pc).borders = [:right, :bottom, :left]

      columns(6..9).align = :center
      columns(11).align = :right
      cells[4, 11].borders = [:right]
      self.row_colors = DOCUMENT_TABLE_ALTERNATE_COLORS
      self.header = true
    end
  end

  def line_contrat_rows
   font_size(8)

   [["No Contrat", "Client", "Date Contrat", "Date Début", "Date Fin", "Type Contrat",
     "Main Oeuvre", "Pièce", "Nbr Incidents", "Nbr Visite", "Intervenant", "Montant"]] +
   @contrats.map do |contrat|
     [contrat.numero, contrat.client.raison_social, contrat.date_creation, contrat.date_debut, contrat.date_fin, truncate(contrat.type_contrat.nom, length: 20),
      contrat.main_oeuvre_presentation, contrat.piece_rechange_presentation, contrat.nombre_incidents, contrat.nombre_visites, contrat.intervenant.nom, price(contrat.montant)]
   end +
   (1..@@pc - @contrats.count).map do |i|
      ["", "", "", "", "", "","", "", "", "", "", ""]
   end
  end

  def price(number)
    @view.number_to_currency(number)
  end

  def months_of_a_year(year)
    move_down(20)
    font_size(11)
    date_from  = DateTime.new(year).beginning_of_year
    date_to    = DateTime.new(year).end_of_year
    date_range = date_from..date_to

    date_months = date_range.map {|d| Date.new(d.year, d.month, 1) }.uniq
    # date_months = date_months.map {|d| d.to_formatted_s(:month_and_year) }
    date_months = date_months.map do |d|
      I18n.l(d, :format => "%b %Y").capitalize
    end

    table([date_months], :width => bounds.width)
  end

  def calc_number_of_pages
    contrat_count = @contrats.count
    @@total_pages = 0
    while contrat_count > 0 do
      @@total_pages += 1
      contrat_count -= DOCUMENT_LINES_COUNT
    end
  end
end