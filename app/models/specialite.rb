class Specialite < ActiveRecord::Base
  default_scope -> { order(nom: :asc) }
end
