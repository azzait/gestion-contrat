class Event
  attr_accessor :title, :start, :end, :allDay, :url, :backgroundColor, :borderColor,
                :numero_contrat, :date_debut, :date_fin, :intervenant, :client, :nombre_visites, :compte_rendu,
                :date_preventive, :date_interventive


  def self.create_events_from_visites(visites)
    events = []
    visites.each do |visite|
      event = self.new
      event.title = visite.date + ' - ' + visite.contrat.numero
      event.start = visite.db_date
      event.backgroundColor = "#25AAE1"

      event.numero_contrat  = visite.contrat.numero
      event.date_debut      = visite.contrat.date_debut
      event.date_fin        = visite.contrat.date_fin
      event.intervenant     = visite.intervenant.nom
      event.client          = visite.contrat.client.raison_social
      event.nombre_visites  = visite.contrat.nombre_visites
      event.compte_rendu    = visite.compte_rendu
      event.date_preventive = visite.date
      event.date_interventive = visite.date_interventive

      events << event
    end
    events
  end

end