class TypeContrat < ActiveRecord::Base
  default_scope -> { order(nom: :asc) }
end
