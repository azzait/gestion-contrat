class Intervenant < ActiveRecord::Base
  belongs_to :qualite
  belongs_to :specialite
  belongs_to :region

  def date_embauche
    I18n.l read_attribute(:date_embauche) if read_attribute(:date_embauche)
  end

end
