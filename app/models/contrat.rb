class Contrat < ActiveRecord::Base
  belongs_to :type_contrat
  belongs_to :client
  belongs_to :intervenant
  has_many :visites, dependent: :destroy

  accepts_nested_attributes_for :visites,
                                allow_destroy: true,
                                reject_if: proc { |attributes| attributes['date'].blank? }

  # default_scope -> { where("numero like '%A%'") }
  default_scope -> { order(numero: :asc) }
  scope :print, -> { includes(:client).order('clients.raison_social ASC') }

  scope :terminate_5_weeks_from_now, ->{ where('date_fin >= ? AND date_fin <= ? ', Date.today, 5.weeks.from_now)}

  validates :numero, :presence => true

  def date_creation
    I18n.l read_attribute(:date_creation) if read_attribute(:date_creation)
  end

  def date_debut
    I18n.l read_attribute(:date_debut) if read_attribute(:date_debut)
  end

  def date_fin
    I18n.l read_attribute(:date_fin) if read_attribute(:date_fin)
  end

  def resile_presentation
    resile ? "Oui" : "Non"
  end

  def main_oeuvre_presentation
    main_oeuvre ? "Oui" : "Non"
  end

  def piece_rechange_presentation
    piece_rechange ? "Oui" : "Non"
  end

  def nombre_incidents
    read_attribute(:nombre_incidents) || 0
  end

  def nombre_visites
    read_attribute(:nombre_visites) || 0
  end

  def visite_of_a_month(month)
    visite = visites.where("strftime('%m', visites.date) = ?", month).first
    visite_color = ''
    visite_color = visite.color.tr('#', '') if visite != nil and visite.color != nil
    if (visite_color != '' and visite_color != 'FFFFFF')
      {:content => visite.nil? ? "" : visite.date, :background_color => visite_color, :text_color => 'FFFFFF'}
    else
      visite.nil? ? "" : visite.date
    end
  end

  def isTerminated
    read_attribute(:date_fin) < Date.today ? true : false
  end

  def contrat_line
    [
        client.raison_social, intervenant.nom, client.contact,
        visite_of_a_month("01"), visite_of_a_month("02"), visite_of_a_month("03"), visite_of_a_month("04"),
        visite_of_a_month("05"), visite_of_a_month("06"), visite_of_a_month("07"), visite_of_a_month("08"),
        visite_of_a_month("09"), visite_of_a_month("10"), visite_of_a_month("11"), visite_of_a_month("12")
    ]
  end

  # def contrat_line
  #   [contrat.client.raison_social, contrat.intervenant.nom, contrat.client.contact,
  #    # "", "", "", "", "", "",
  #    # "", "", "", "", "", ""]
  #    #
  #    contrat.visite_of_a_month("01"), contrat.visite_of_a_month("02"), contrat.visite_of_a_month("03"), contrat.visite_of_a_month("04"),
  #    contrat.visite_of_a_month("05"), contrat.visite_of_a_month("06"), contrat.visite_of_a_month("07"), contrat.visite_of_a_month("08"),
  #    contrat.visite_of_a_month("09"), contrat.visite_of_a_month("10"), contrat.visite_of_a_month("11"), contrat.visite_of_a_month("12")]
  # end

end
