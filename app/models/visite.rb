class Visite < ActiveRecord::Base
  # belongs_to :client
  belongs_to :contrat
  belongs_to :intervenant

  default_scope -> { where('date not null').order(date: :asc) }

  scope :visites_by_this_month, ->{ where("strftime('%m', visites.date) = ?", "%02d" % Date.today.month)}
  scope :visites_by_today, ->{ where("date = ?", Date.today)}


  def date
    I18n.l read_attribute(:date) if read_attribute(:date)
  end

  def date_interventive
    I18n.l read_attribute(:date_interventive) if read_attribute(:date_interventivex)
  end

  def db_date
    read_attribute(:date)
  end


end
