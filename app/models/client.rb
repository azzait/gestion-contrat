class Client < ActiveRecord::Base
  belongs_to :region

  default_scope -> { order(raison_social: :asc) }

  def raison_social
    read_attribute(:raison_social).to_s.upcase
  end
end
