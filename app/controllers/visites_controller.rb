class VisitesController < ApplicationController
  # belongs_to 'contrat', :find_by => :id
  before_action :set_visite, only: [:show, :edit, :update, :destroy]

  # GET /visites
  # GET /visites.json
  def index
    @contrat = Contrat.find(params[:contrat_id])
    @intervenants = Intervenant.all
    @visites = @contrat.visites
    @contrat.visites.build
   # @tst_visite =  @contrat.visites.build
    @colors = ["Red" => "#f00", "Blue" => "blue"]
  end

  def index2
    @visites = []
    @contrat = Contrat.find(params[:contrat_id])
    @visites = @contrat.visites

    if @contrat.te
      
    end

    # @intervenants = Intervenant.all
    #
    #
    # for i in 1..@contrat.nombre_visites
    #   # Visite.find(contrat_id: @contrat.id, position: i)
    #   visite = @contrat.visites.build if visite.nil?
    #   visite.position = i
    #   visite.contrat = @contrat
    #   @visites << visite
    # end
  end



  # GET /visites/1
  # GET /visites/1.json
  def show
  end

  # GET /visites/new
  def new
    contrat = Contrat.find(params[:contrat_id])
    @visite = contrat.visites.build
  end

  # GET /visites/1/edit
  def edit
  end

  # POST /visites
  # POST /visites.json
  def create
    @visite = Visite.new(visite_params)

    respond_to do |format|
      if @visite.save
        format.html { redirect_to root_url, notice: 'Visite was successfully created.' }
        format.json { render :show, status: :created, location: @visite }
      else
        format.html { render :new }
        format.json { render json: @visite.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /visites/1
  # PATCH/PUT /visites/1.json
  def update
    respond_to do |format|
      if @visite.update(visite_params)
        format.html { redirect_to contrats_url, notice: 'Visite was successfully updated.' }
        format.json { render :show, status: :ok, location: @visite }
      else
        format.html { render :edit }
        format.json { render json: @visite.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /visites/1
  # DELETE /visites/1.json
  def destroy
    @visite.destroy
    respond_to do |format|
      format.html { redirect_to visites_url, notice: 'Visite was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_visite
      @contrat = Contrat.find(params[:contrat_id])
      @visite = @contrat.visites.find(params[:id])

      # @visite = Visite.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def visite_params
      params.require(:visite).permit!
    end
end
