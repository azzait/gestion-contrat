class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  before_action :setup_notifications

  layout :layout_by_resource

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception


  def setup_notifications
    @not1_visites_by_today = Visite.visites_by_today
    @not1_visites_by_this_month = Visite.visites_by_this_month
    @not1_terminate_5_weeks_from_now = Contrat.terminate_5_weeks_from_now

    @notification_count = @not1_visites_by_today.count + @not1_visites_by_this_month.count + @not1_terminate_5_weeks_from_now.count
  end



  protected

    def layout_by_resource
      if devise_controller? && resource_name == :user && (action_name == "new" or action_name == "create")
        "admin_lte_2_register"
      else
        "admin_lte_2"
      end
    end

  private

    # Overwriting the sign_out redirect path method
    def after_sign_out_path_for(resource_or_scope)
      new_user_session_path
    end
end
