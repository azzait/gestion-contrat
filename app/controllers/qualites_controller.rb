class QualitesController < ApplicationController
  before_action :set_qualite, only: [:show, :edit, :update, :destroy]

  # GET /qualites
  # GET /qualites.json
  def index
    @qualites = Qualite.all
  end

  # GET /qualites/1
  # GET /qualites/1.json
  def show
  end

  # GET /qualites/new
  def new
    @qualite = Qualite.new
  end

  # GET /qualites/1/edit
  def edit
  end

  # POST /qualites
  # POST /qualites.json
  def create
    @qualite = Qualite.new(qualite_params)

    respond_to do |format|
      if @qualite.save
        format.html { redirect_to qualites_url, notice: 'Qualite a été créé avec succès.' }
        format.json { render :show, status: :created, location: @qualite }
      else
        format.html { render :new }
        format.json { render json: @qualite.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /qualites/1
  # PATCH/PUT /qualites/1.json
  def update
    respond_to do |format|
      if @qualite.update(qualite_params)
        format.html { redirect_to qualites_url, notice: 'Qualite  a été mis à jour avec succès.' }
        format.json { render :show, status: :ok, location: @qualite }
      else
        format.html { render :edit }
        format.json { render json: @qualite.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qualites/1
  # DELETE /qualites/1.json
  def destroy
    @qualite.destroy
    respond_to do |format|
      format.html { redirect_to qualites_url, notice: 'Qualite a été supprimée avec succès.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_qualite
      @qualite = Qualite.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def qualite_params
      params.require(:qualite).permit(:nom)
    end
end
