class SpecialitesController < ApplicationController
  before_action :set_specialite, only: [:show, :edit, :update, :destroy]

  # GET /specialites
  # GET /specialites.json
  def index
    @specialites = Specialite.all
  end

  # GET /specialites/1
  # GET /specialites/1.json
  def show
  end

  # GET /specialites/new
  def new
    @specialite = Specialite.new
  end

  # GET /specialites/1/edit
  def edit
  end

  # POST /specialites
  # POST /specialites.json
  def create
    @specialite = Specialite.new(specialite_params)

    respond_to do |format|
      if @specialite.save
        format.html { redirect_to specialites_path, notice: 'Specialite a été créé avec succès.' }
        format.json { render :index, status: :created, location: @specialite }
      else
        format.html { render :new }
        format.json { render json: @specialite.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /specialites/1
  # PATCH/PUT /specialites/1.json
  def update
    respond_to do |format|
      if @specialite.update(specialite_params)
        format.html { redirect_to specialites_path, notice: 'Specialite  a été mis à jour avec succès.' }
        format.json { render :index, status: :ok, location: @specialite }
      else
        format.html { render :edit }
        format.json { render json: @specialite.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /specialites/1
  # DELETE /specialites/1.json
  def destroy
    @specialite.destroy
    respond_to do |format|
      format.html { redirect_to specialites_url, notice: 'Specialite a été supprimée avec succès.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_specialite
      @specialite = Specialite.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def specialite_params
      params.require(:specialite).permit(:nom)
    end
end
