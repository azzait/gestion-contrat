class ContratsController < ApplicationController
  before_action :set_contrat, only: [:show, :edit, :update, :destroy]
  before_action :set_contrats, only: [:index, :print_visites, :apercu]

  # GET /contrats
  # GET /contrats.json
  def index

  end

  # GET /print_contrats
  def print_contrats
    @contrats = Contrat.print
    respond_to do |format|
      format.html
      format.pdf do
        pdf = ContratsPDF.new(@contrats, view_context)
        send_data pdf.render, filename: "contrats.pdf",
                  type: "application/pdf",
                  disposition: "inline"
      end
    end
  end

  # GET /print_visites
  def print_visites
    respond_to do |format|
      format.html
      format.pdf do
        pdf = VisitesPDF.new(@contrats, view_context)
        send_data pdf.render, filename: "visites.pdf",
                  type: "application/pdf",
                  disposition: "inline"
      end
    end
  end

  # GET /contrats
  def apercu
    respond_to do |format|
      format.html
      format.pdf do
        pdf = ContratsPDF.new(@contrats, view_context)
        send_data pdf.render, filename: "contrats.pdf",
                  type: "application/pdf",
                  disposition: "inline"
      end
    end
  end

  # GET /contrats/1
  # GET /contrats/1.json
  def show
  end

  # GET /contrats/new
  def new
    @contrat = Contrat.new
  end

  # GET /contrats/1/edit
  def edit
  end

  # POST /contrats
  # POST /contrats.json
  def create
    @contrat = Contrat.new(contrat_params)

    respond_to do |format|
      if @contrat.save
        format.html { redirect_to contrats_url, notice: 'Contrat a été créé avec succès.' }
        format.json { render :show, status: :created, location: @contrat }
      else
        format.html { render :new }
        format.json { render json: @contrat.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contrats/1
  # PATCH/PUT /contrats/1.json
  def update
    respond_to do |format|
      if @contrat.update(contrat_params)
        format.html { redirect_to contrats_url, notice: 'Contrat  a été mis à jour avec succès.' }
        format.json { render :show, status: :ok, location: @contrat }
      else
        format.html { render :edit }
        format.json { render json: @contrat.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contrats/1
  # DELETE /contrats/1.json
  def destroy
    @contrat.destroy
    respond_to do |format|
      format.html { redirect_to contrats_url, notice: 'Contrat a été supprimé avec succès.' }
      format.json { head :no_content }
    end
  end

  def update_main_oeuvre
    model_class.where(:id => params[:id]).update_all(:main_oeuvre => params[:main_oeuvre] == "true" ? '1' : '0')

    respond_to do |format|
      format.js  { render :text => 'Ok' }
    end
  end

  def update_piece_rechange
    model_class.where(:id => params[:id]).update_all(:piece_rechange => params[:piece_rechange] == "true" ? '1' : '0')

    respond_to do |format|
      format.js  { render :text => 'Ok' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contrat
      @contrat = Contrat.find(params[:id])
    end

    def set_contrats
      @contrats = Contrat.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contrat_params
      params.require(:contrat).permit!
    end
end