class RegistrationsController < Devise::RegistrationsController
  prepend_before_action :require_no_authentication, only: [:new, :create, :cancel]
  # skip_before_filter :authenticate_user!

  def new
    super
  end

  def create
    super
  end

  def update
    super
  end

  private

    def sign_up_params
      params.require(:user).permit(:nom, :email, :password, :password_confirmation)
    end

    def account_update_params
      params.require(:user).permit(:nom, :email, :password, :password_confirmation, :current_password)
    end
end