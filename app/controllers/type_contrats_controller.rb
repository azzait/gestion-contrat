class TypeContratsController < ApplicationController
  before_action :set_type_contrat, only: [:show, :edit, :update, :destroy]

  # GET /type_contrats
  # GET /type_contrats.json
  def index
    @type_contrats = TypeContrat.all
  end

  # GET /type_contrats/1
  # GET /type_contrats/1.json
  def show
  end

  # GET /type_contrats/new
  def new
    @type_contrat = TypeContrat.new
  end

  # GET /type_contrats/1/edit
  def edit
  end

  # POST /type_contrats
  # POST /type_contrats.json
  def create
    @type_contrat = TypeContrat.new(type_contrat_params)

    respond_to do |format|
      if @type_contrat.save
        format.html { redirect_to type_contrats_url, notice: 'Type contrat a été créé avec succès.' }
        format.json { render :show, status: :created, location: @type_contrat }
      else
        format.html { render :new }
        format.json { render json: @type_contrat.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /type_contrats/1
  # PATCH/PUT /type_contrats/1.json
  def update
    respond_to do |format|
      if @type_contrat.update(type_contrat_params)
        format.html { redirect_to type_contrats_url, notice: 'Type contrat  a été mis à jour avec succès.' }
        format.json { render :show, status: :ok, location: @type_contrat }
      else
        format.html { render :edit }
        format.json { render json: @type_contrat.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /type_contrats/1
  # DELETE /type_contrats/1.json
  def destroy
    @type_contrat.destroy
    respond_to do |format|
      format.html { redirect_to type_contrats_url, notice: 'Type contrat a été supprimé avec succès.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_type_contrat
      @type_contrat = TypeContrat.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def type_contrat_params
      params.require(:type_contrat).permit(:nom, :nombre_intervention)
    end
end
