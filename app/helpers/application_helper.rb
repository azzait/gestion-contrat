module ApplicationHelper

  def set_box_width
    content_for(:box_width) || "col-md-6"
  end

  def options_with_colors(colors)
    options = []
    colors.collect do |color, code|
      options <<  "<option value='#{code}' style='background-color:#{code};'>#{color}</option> "
    end
    options
  end

  def options_for_select_with_color( container, selected=nil )
    container = [['Normal', 'default', '#FFFFFF'],
    ['Moyenne', 'warning', '#F39C12'],
    ['Élevé', 'red', '#DD4B39']]
    container = container.to_a if Hash === container
    options_for_select = container.inject([]) do |options, element|
      text, value = option_text_and_value(element)
      state = element[1]
      selected_attribute = ' selected' if option_value_selected?(value, selected)
      # style = " style=\"#{element[1]}\"" if element[1] && element[1]!=value
      options << %(<option value=#{value} data-icon="fa fa-square #{html_escape(state.to_s)}" #{selected_attribute}>#{html_escape(text.to_s)}</option>)
    end
    raw options_for_select.join("\n")
  end
end
