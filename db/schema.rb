# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160811094854) do

  create_table "clients", force: :cascade do |t|
    t.string   "code"
    t.string   "raison_social"
    t.text     "adresse"
    t.string   "telephone"
    t.string   "fax"
    t.string   "contact"
    t.string   "email"
    t.integer  "region_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "clients", ["region_id"], name: "index_clients_on_region_id"

  create_table "contrats", force: :cascade do |t|
    t.string   "numero"
    t.date     "date_creation"
    t.date     "date_debut"
    t.date     "date_fin"
    t.boolean  "main_oeuvre"
    t.integer  "nombre_incidents"
    t.boolean  "piece_rechange"
    t.decimal  "montant"
    t.boolean  "resile"
    t.text     "cause_resile"
    t.integer  "type_contrat_id"
    t.integer  "client_id"
    t.integer  "intervenant_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "nombre_visites"
  end

  add_index "contrats", ["client_id"], name: "index_contrats_on_client_id"
  add_index "contrats", ["intervenant_id"], name: "index_contrats_on_intervenant_id"
  add_index "contrats", ["type_contrat_id"], name: "index_contrats_on_type_contrat_id"

  create_table "intervenants", force: :cascade do |t|
    t.string   "code"
    t.string   "nom"
    t.text     "adresse"
    t.string   "telephone"
    t.string   "email"
    t.integer  "cout_horaire"
    t.string   "mot_de_passe"
    t.date     "date_embauche"
    t.integer  "qualite_id"
    t.integer  "specialite_id"
    t.integer  "region_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "intervenants", ["qualite_id"], name: "index_intervenants_on_qualite_id"
  add_index "intervenants", ["region_id"], name: "index_intervenants_on_region_id"
  add_index "intervenants", ["specialite_id"], name: "index_intervenants_on_specialite_id"

  create_table "qualites", force: :cascade do |t|
    t.string   "nom"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "regions", force: :cascade do |t|
    t.string   "nom"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "specialites", force: :cascade do |t|
    t.string   "nom"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "type_contrats", force: :cascade do |t|
    t.string   "nom"
    t.integer  "nombre_intervention"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "nom"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

  create_table "visites", force: :cascade do |t|
    t.integer  "contrat_id"
    t.integer  "intervenant_id"
    t.date     "date"
    t.text     "compte_rendu"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "position"
    t.string   "color"
    t.date     "date_interventive"
  end

  add_index "visites", ["contrat_id"], name: "index_visites_on_contrat_id"
  add_index "visites", ["intervenant_id"], name: "index_visites_on_intervenant_id"

end
