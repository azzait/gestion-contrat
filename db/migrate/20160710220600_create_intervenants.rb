class CreateIntervenants < ActiveRecord::Migration
  def change
    create_table :intervenants do |t|
      t.string :code
      t.string :nom
      t.text :adresse
      t.string :telephone
      t.string :email
      t.integer :cout_horaire
      t.string :mot_de_passe
      t.date :date_embauche
      t.references :qualite, index: true, foreign_key: true
      t.references :specialite, index: true, foreign_key: true
      t.references :region, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
