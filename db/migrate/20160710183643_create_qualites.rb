class CreateQualites < ActiveRecord::Migration
  def change
    create_table :qualites do |t|
      t.string :nom

      t.timestamps null: false
    end
  end
end
