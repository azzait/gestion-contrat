class RemoveClientToVisites < ActiveRecord::Migration
  def change
    remove_reference :visites, :client, index: true, foreign_key: true
  end
end
