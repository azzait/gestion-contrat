class CreateSpecialites < ActiveRecord::Migration
  def change
    create_table :specialites do |t|
      t.string :nom

      t.timestamps null: false
    end
  end
end
