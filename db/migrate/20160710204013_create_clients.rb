class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :code
      t.string :raison_social
      t.text :adresse
      t.string :telephone
      t.string :fax
      t.string :contact
      t.string :email
      t.references :region, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
