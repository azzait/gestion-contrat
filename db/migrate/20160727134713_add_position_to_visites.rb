class AddPositionToVisites < ActiveRecord::Migration
  def change
    add_column :visites, :position, :integer
  end
end
