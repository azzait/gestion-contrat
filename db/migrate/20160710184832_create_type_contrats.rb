class CreateTypeContrats < ActiveRecord::Migration
  def change
    create_table :type_contrats do |t|
      t.string :nom
      t.integer :nombre_intervention

      t.timestamps null: false
    end
  end
end
