class AddDateInterventiveToVisites < ActiveRecord::Migration
  def change
    add_column :visites, :date_interventive, :date
  end
end
