class AddColorToVisites < ActiveRecord::Migration
  def change
    add_column :visites, :color, :string
  end
end
