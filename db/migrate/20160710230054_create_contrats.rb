class CreateContrats < ActiveRecord::Migration
  def change
    create_table :contrats do |t|
      t.string :numero
      t.date :date_creation
      t.date :date_debut
      t.date :date_fin
      t.boolean :main_oeuvre
      t.integer :nombre_incidents
      t.boolean :piece_rechange
      t.decimal :montant
      t.boolean :resile
      t.text :cause_resile
      t.references :type_contrat, index: true, foreign_key: true
      t.references :client, index: true, foreign_key: true
      t.references :intervenant, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
