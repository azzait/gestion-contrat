class CreateVisites < ActiveRecord::Migration
  def change
    create_table :visites do |t|
      t.references :client, index: true, foreign_key: true
      t.references :contrat, index: true, foreign_key: true
      t.references :intervenant, index: true, foreign_key: true
      t.date :date
      t.text :compte_rendu

      t.timestamps null: false
    end
  end
end
