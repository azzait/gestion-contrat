require 'test_helper'

class VisitesControllerTest < ActionController::TestCase
  setup do
    @visite = visites(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:visites)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create visite" do
    assert_difference('Visite.count') do
      post :create, visite: { client_id: @visite.client_id, compte_rendu: @visite.compte_rendu, contrat_id: @visite.contrat_id, date: @visite.date, intervenant_id: @visite.intervenant_id }
    end

    assert_redirected_to visite_path(assigns(:visite))
  end

  test "should show visite" do
    get :show, id: @visite
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @visite
    assert_response :success
  end

  test "should update visite" do
    patch :update, id: @visite, visite: { client_id: @visite.client_id, compte_rendu: @visite.compte_rendu, contrat_id: @visite.contrat_id, date: @visite.date, intervenant_id: @visite.intervenant_id }
    assert_redirected_to visite_path(assigns(:visite))
  end

  test "should destroy visite" do
    assert_difference('Visite.count', -1) do
      delete :destroy, id: @visite
    end

    assert_redirected_to visites_path
  end
end
