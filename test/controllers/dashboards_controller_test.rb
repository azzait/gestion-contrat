require 'test_helper'

class DashboardsControllerTest < ActionController::TestCase
  test "should get tableau_board" do
    get :tableau_board
    assert_response :success
  end

  test "should get visites_preventive" do
    get :visites_preventive
    assert_response :success
  end

end
