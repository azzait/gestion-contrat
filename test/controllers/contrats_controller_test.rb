require 'test_helper'

class ContratsControllerTest < ActionController::TestCase
  setup do
    @contrat = contrats(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:contrats)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create contrat" do
    assert_difference('Contrat.count') do
      post :create, contrat: { cause_resile: @contrat.cause_resile, client_id: @contrat.client_id, date_creation: @contrat.date_creation, date_debut: @contrat.date_debut, date_fin: @contrat.date_fin, intervenant_id: @contrat.intervenant_id, main_oeuvre: @contrat.main_oeuvre, montant: @contrat.montant, nombre_incidents: @contrat.nombre_incidents, numero: @contrat.numero, piece_rechange: @contrat.piece_rechange, resile: @contrat.resile, type_contrat_id: @contrat.type_contrat_id }
    end

    assert_redirected_to contrat_path(assigns(:contrat))
  end

  test "should show contrat" do
    get :show, id: @contrat
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @contrat
    assert_response :success
  end

  test "should update contrat" do
    patch :update, id: @contrat, contrat: { cause_resile: @contrat.cause_resile, client_id: @contrat.client_id, date_creation: @contrat.date_creation, date_debut: @contrat.date_debut, date_fin: @contrat.date_fin, intervenant_id: @contrat.intervenant_id, main_oeuvre: @contrat.main_oeuvre, montant: @contrat.montant, nombre_incidents: @contrat.nombre_incidents, numero: @contrat.numero, piece_rechange: @contrat.piece_rechange, resile: @contrat.resile, type_contrat_id: @contrat.type_contrat_id }
    assert_redirected_to contrat_path(assigns(:contrat))
  end

  test "should destroy contrat" do
    assert_difference('Contrat.count', -1) do
      delete :destroy, id: @contrat
    end

    assert_redirected_to contrats_path
  end
end
