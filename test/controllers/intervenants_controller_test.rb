require 'test_helper'

class IntervenantsControllerTest < ActionController::TestCase
  setup do
    @intervenant = intervenants(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:intervenants)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create intervenant" do
    assert_difference('Intervenant.count') do
      post :create, intervenant: { adresse: @intervenant.adresse, code: @intervenant.code, cout_horaire: @intervenant.cout_horaire, date_embauche: @intervenant.date_embauche, email: @intervenant.email, mot_de_passe: @intervenant.mot_de_passe, nom: @intervenant.nom, qualite_id: @intervenant.qualite_id, region_id: @intervenant.region_id, specialite_id: @intervenant.specialite_id, telephone: @intervenant.telephone }
    end

    assert_redirected_to intervenant_path(assigns(:intervenant))
  end

  test "should show intervenant" do
    get :show, id: @intervenant
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @intervenant
    assert_response :success
  end

  test "should update intervenant" do
    patch :update, id: @intervenant, intervenant: { adresse: @intervenant.adresse, code: @intervenant.code, cout_horaire: @intervenant.cout_horaire, date_embauche: @intervenant.date_embauche, email: @intervenant.email, mot_de_passe: @intervenant.mot_de_passe, nom: @intervenant.nom, qualite_id: @intervenant.qualite_id, region_id: @intervenant.region_id, specialite_id: @intervenant.specialite_id, telephone: @intervenant.telephone }
    assert_redirected_to intervenant_path(assigns(:intervenant))
  end

  test "should destroy intervenant" do
    assert_difference('Intervenant.count', -1) do
      delete :destroy, id: @intervenant
    end

    assert_redirected_to intervenants_path
  end
end
