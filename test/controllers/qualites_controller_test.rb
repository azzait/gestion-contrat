require 'test_helper'

class QualitesControllerTest < ActionController::TestCase
  setup do
    @qualite = qualites(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qualites)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qualite" do
    assert_difference('Qualite.count') do
      post :create, qualite: { nom: @qualite.nom }
    end

    assert_redirected_to qualite_path(assigns(:qualite))
  end

  test "should show qualite" do
    get :show, id: @qualite
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qualite
    assert_response :success
  end

  test "should update qualite" do
    patch :update, id: @qualite, qualite: { nom: @qualite.nom }
    assert_redirected_to qualite_path(assigns(:qualite))
  end

  test "should destroy qualite" do
    assert_difference('Qualite.count', -1) do
      delete :destroy, id: @qualite
    end

    assert_redirected_to qualites_path
  end
end
