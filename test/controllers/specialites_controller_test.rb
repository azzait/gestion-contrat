require 'test_helper'

class SpecialitesControllerTest < ActionController::TestCase
  setup do
    @specialite = specialites(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:specialites)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create specialite" do
    assert_difference('Specialite.count') do
      post :create, specialite: { nom: @specialite.nom }
    end

    assert_redirected_to specialite_path(assigns(:specialite))
  end

  test "should show specialite" do
    get :show, id: @specialite
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @specialite
    assert_response :success
  end

  test "should update specialite" do
    patch :update, id: @specialite, specialite: { nom: @specialite.nom }
    assert_redirected_to specialite_path(assigns(:specialite))
  end

  test "should destroy specialite" do
    assert_difference('Specialite.count', -1) do
      delete :destroy, id: @specialite
    end

    assert_redirected_to specialites_path
  end
end
