require 'test_helper'

class TypeContratsControllerTest < ActionController::TestCase
  setup do
    @type_contrat = type_contrats(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:type_contrats)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create type_contrat" do
    assert_difference('TypeContrat.count') do
      post :create, type_contrat: { nom: @type_contrat.nom, nombre_intervention: @type_contrat.nombre_intervention }
    end

    assert_redirected_to type_contrat_path(assigns(:type_contrat))
  end

  test "should show type_contrat" do
    get :show, id: @type_contrat
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @type_contrat
    assert_response :success
  end

  test "should update type_contrat" do
    patch :update, id: @type_contrat, type_contrat: { nom: @type_contrat.nom, nombre_intervention: @type_contrat.nombre_intervention }
    assert_redirected_to type_contrat_path(assigns(:type_contrat))
  end

  test "should destroy type_contrat" do
    assert_difference('TypeContrat.count', -1) do
      delete :destroy, id: @type_contrat
    end

    assert_redirected_to type_contrats_path
  end
end
