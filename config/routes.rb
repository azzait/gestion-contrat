Rails.application.routes.draw do

  root 'dashboards#visites_preventive'

  get 'dashboards/tableau_board'
  get 'dashboards/visites_preventive'

  resources :intervenants
  resources :clients
  resources :type_contrats
  resources :qualites
  resources :regions
  resources :specialites

  # devise_for :users
  devise_for :users, :controllers => { registrations: 'registrations' },
      :path => '', :path_names => {:sign_in => 'login', :sign_out => 'logout', :sign_up => 'register'}


  get 'print_contrats' => 'contrats#print_contrats'
  get 'print_visites' => 'contrats#print_visites'
  get 'apercu' => 'contrats#apercu'


  resources :contrats do
    collection {
      post :update_main_oeuvre
      post :update_piece_rechange
      # post :import
    }
    resources :visites
    get 'index2' => 'visites#index2'
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
