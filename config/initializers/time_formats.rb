
Time::DATE_FORMATS[:month_and_year] = "%B-%Y"
Time::DATE_FORMATS[:short_ordinal] = lambda { |time| time.strftime("%B #{time.day.ordinalize}") }
Date::DATE_FORMATS[:month_and_year] = "%b-%Y"
Date::DATE_FORMATS[:short_ordinal] = lambda { |time| time.strftime("%B #{time.day.ordinalize}") }